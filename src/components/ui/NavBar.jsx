import React, { useState } from "react";
import { useSelector } from "react-redux";
import image from "../../assets/icon.png";
import { ProductInCart } from "../../pages/Products/ProductInCart";

export function NavBar() {
  const [isVisible, setVisible] = useState(false);
  const cart = useSelector((state) => state.cart);
  const toggle = () => {
    setVisible(!isVisible);
  };
  return (
    <>
      <nav style={{ backgroundColor: "#0D0C1D" }} class="navbar  h-100 w-100">
        <div class="container-fluid ">
          <div class="d-flex flex-row w-100 justify-content-evenly">
            <div class="d-flex w-25 justify-content-start">
              <a class="navbar-brand" href="/">
                <img
                  src="https://www.pngmart.com/files/Anime-Food-Download-PNG-Image.png"
                  alt="Bootstrap"
                  width="40"
                  height="40"
                ></img>
                <span
                  style={{
                    color: "white",
                    fontFamily: "Serif",
                  }}
                >
                  Fukusuke
                </span>
              </a>
            </div>
            <div class="d-flex flex-fill d-flex justify-content-end w-75">
              <div class=" d-flex flex-fill justify-content-around w-100">
                <a
                  href="/menu"
                  style={{
                    backgroundColor: "#0D0C1D",
                    borderColor: "#0D0C1D",
                    color: "#A69CAC",
                  }}
                  class="btn btn-dark btn-secondary btn-lg active"
                  role="button"
                  aria-pressed="true"
                >
                  Menú
                </a>

                <button
                  style={{
                    backgroundColor: "#0D0C1D",
                    borderColor: "#0D0C1D",
                    color: "#A69CAC",
                  }}
                  type="button"
                  class="btn btn-primary"
                >
                  Repetir pedido
                </button>
                <a
                  href="/login"
                  style={{
                    backgroundColor: "#0D0C1D",
                    borderColor: "#0D0C1D",
                    color: "#A69CAC",
                  }}
                  class="btn btn-dark btn-secondary btn-lg active"
                  role="button"
                  aria-pressed="true"
                >
                  Login
                </a>
                <button
                  style={{
                    backgroundColor: "#0D0C1D",
                    borderColor: "#0D0C1D",
                    color: "#A69CAC",
                  }}
                  class="btn btn-primary"
                  type="button"
                  data-bs-toggle="offcanvas"
                  data-bs-target="#offcanvasRight"
                  aria-controls="offcanvasRight"
                >
                  {cart.products.length > 0 ? (
                    <i class="fa-solid fa-cart-plus"></i>
                  ) : (
                    <i class="fa fa-cart-shopping"></i>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <div
        class="offcanvas offcanvas-end"
        tabindex="-1"
        id="offcanvasRight"
        aria-labelledby="offcanvasRightLabel"
      >
        <div class="offcanvas-header">
          <h5 class="offcanvas-title" id="offcanvasRightLabel">
            Carrito de compras
          </h5>

          <button
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasRight"
            aria-controls="offcanvasRight "
            type="button"
            class="btn-close"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <div class="offcanvas-body">
          {cart.products.length == 0 && (
            <div className="d-flex justify-content-center align-items-center">
              El carrito está vacío
            </div>
          )}
          {cart.products.length > 0 && (
            <div className="d-flex flex-column justify-content-center align-items-center">
              {" "}
              {cart.products.map((product) => (
                <ProductInCart props={product} />
              ))}
              <div className="d-flex flex-row mt-5 gap-5">
                <div className="d-flex align-items-center">
                  <button
                    style={{
                      alignItems: "center",
                      borderRadius: "50%",
                      background: "red",
                      border: "none",
                      width: "40px",
                      height: "40px",
                    }}
                    type="button"
                    class="btn btn-primary"
                  >
                    <i class="fa-solid fa-times"></i>
                  </button>
                </div>
                <div>
                  <button
                    style={{
                      borderRadius: "50%",
                      background: "green",
                      border: "none",
                      width: "40px",
                      height: "40px",
                    }}
                    type="button"
                    class="btn btn-primary"
                  >
                    <i class="fa-solid fa-check"></i>
                  </button>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
