import { createSlice } from "@reduxjs/toolkit";
const initialState ={
    products:[],
}
export const cartSlice = createSlice({
    name:'cart',
    initialState,
    reducers:{
        
        addProduct: (state,action) =>{
            let newProduct = action.payload
            if (!newProduct.hasOwnProperty('cant')){
                newProduct['cant'] = 1
            }
            
            
            
            const i = state.products.findIndex(product=>product.id==newProduct.id)
            // const i2 = state.products.indexOf()>-1
            
            let copy = state.products

            if(i>=0){
                copy[i].cant+=1
                
                state.products = copy
                return
            }
            state.products = [...state.products,newProduct]
            return
       
            
            // for (let product in state.products){
                
            //     if (product.id == action.payload.id){
            //         product.cant+=1
            //     }
            //     else{
            //         newProduct['cant']=1
            //     }
            
            

                
            
        },
        minusQuantity:(state,action)=>{
            let newProduct = action.payload
           
            
            
            const i = state.products.findIndex(product=>product.id==newProduct.id)
            
            // const i2 = state.products.indexOf()>-1
            
            let copy = state.products

            if(i>=0){
                copy[i].cant-=1
                if(copy[i].cant==0){
                    const index = copy.indexOf(copy[i]);
                        if (index > -1) { // only splice array when item is found
                        copy.splice(index, 1); // 2nd parameter means remove one item only
                        }

                }
                state.products = copy
                return
            }
        
        },
        plusQuantity:(state,action)=>{
            let newProduct = action.payload
            if (!newProduct.hasOwnProperty('cant')){
                newProduct['cant'] = 0
            }
            
            
            const i = state.products.findIndex(product=>product.id==newProduct.id)
            // const i2 = state.products.indexOf()>-1
            
            let copy = state.products

            if(i>=0){
                copy[i].cant+=1
                
                state.products = copy
                return
            }
        },
        emptyCart:(state,action)=>{
            let copy = state.products
            copy.splice(0)
            state.products = copy
            return
        }
    }
})
export const { addProduct,plusQuantity,minusQuantity,emptyCart } = cartSlice.actions

export default cartSlice.reducer