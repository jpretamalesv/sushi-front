import React from "react";
import { NavBar } from "../../components/ui/NavBar";
import { Footer } from "../../components/ui/Footer";
import { useState } from "react";
import { AddressEditor } from "./AddressEditor";
import { PreviousOrders } from "./PreviousOrders";

export function ProfileEditor() {
  const [profileVisible, setDirVisible] = useState(true);
  const [ordersVisible, setOrderVisible] = useState(false);

  const toggleProfile = () => {
    setDirVisible(true);
    setOrderVisible(false);
  };
  const toggleOrders = () => {
    setDirVisible(false);
    setOrderVisible(true);
  };

  
  return (
    <div className="d-flex flex-column h-100 justify-content-between">
      <div>
        <NavBar />
      </div>
      <div className="d-flex flex-column w-100 h-100">
        <div className="row w-100 h-100">
          <div className="col-3">
            <div className="d-flex flex-column align-items-center justify-content-center h-100">
              <button
                onClick={toggleProfile}
                style={{
                  width: "90%",
                  borderRadius: "0%",
                  background: "purple",
                  border: "none",
                }}
                type="button"
                class="btn btn-primary "
              >
                Perfil
              </button>
              <button
                onClick={toggleOrders}
                style={{
                  width: "90%",
                  borderRadius: "0%",
                  background: "purple",
                  border: "none",
                }}
                type="button"
                class="btn btn-primary "
              >
                Pedidos
              </button>
              <button
                style={{
                  width: "90%",
                  borderRadius: "0%",
                  background: "purple",
                  border: "none",
                }}
                type="button"
                class="btn btn-primary"
              >
                Auxiliar
              </button>
            </div>
          </div>{" "}
          {profileVisible && (
            <div className="col-9 justify-content-center align-items-center d-flex ">
              <div
                style={{ background: "whitesmoke", border: "solid 5px purple" }}
                className=" w-75 h-75"
              >
                <AddressEditor />
              </div>
            </div>
          )}
          {ordersVisible && (
            <div className="col-9 justify-content-center align-items-center d-flex">
              <div className=" w-75 h-75 ">
                <PreviousOrders />
              </div>
            </div>
          )}
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
