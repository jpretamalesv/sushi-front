import React from "react";
import ReactDom from "react-dom";

const MODAL_STYLES = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "50px",
  zIndex: 3000,
  borderRadius: "10px",
  width: "250px",
  border: "solid 1px black",
};
const OVERLAY_STYLES = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,.7)",
  zIndex: 2000,
};

export default function Modal({ open, children, onClose }) {
  if (!open) return null;
  return ReactDom.createPortal(
    <>
      <div style={OVERLAY_STYLES} />
      <div
        style={MODAL_STYLES}
        className="d-flex flex-column gap-3 mw-75 mw-lg-25"
      >
        <div className="d-flex w-100 justify-content-between">
          <h4>Confirmacion</h4>
          <button
            onClick={onClose}
            type="button"
            class="btn-close"
            aria-label="Close"
          ></button>
        </div>
        <div className="d-flex flex-row justify-content-center">{children}</div>
        <div className="d-flex flex-row justify-content-between">
          <button
            style={{ border: "none", background: "red", width: "95px" }}
            type="button"
            class="btn btn-primary"
          >
            Cancelar
          </button>
          <button
            style={{ width: "95px" }}
            type="button"
            class="btn btn-primary confirmButton"
          >
            Confirmar
          </button>
        </div>
      </div>
    </>,
    document.getElementById("portal")
  );
}
