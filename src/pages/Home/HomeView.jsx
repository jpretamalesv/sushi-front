import React from "react";
import { useState } from "react";
import { Footer } from "../../components/ui/Footer";
import { NavBar } from "../../components/ui/NavBar";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { Login } from "../Login/Login";
import image from "../../assets/promoex.png";
import { ProductCard } from "../Products/ProductCard";
import data from "../../data/product.json";

export function Home() {
  const [isVisible, setVisible] = useState(false);
  const toggle = () => {
    setVisible(!isVisible);
  };

  return (
    <div class="d-flex flex-column h-100 w-100 justify-content-between p-0">
      <div className="d-flex d-none d-lg-block">
        <NavBar />
      </div>
      <div className="d-flex d-lg-none d-sm-block">
        <NavBarSm />
      </div>
      <div className="d-flex flex-column">
        <div className="d-flex flex-column  align-items-center justify-content-center p-0">
          <div
            class="card mh-75 mw-75"
            style={{
              background: "none",
              border: "none",
              padding: "0px",
              width: "50%",
            }}
          >
            <div class="card-body">
              <div
                id="carouselExampleDark"
                class="carousel carousel-white slide"
                data-bs-ride="carousel"
              >
                <div class="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleDark"
                    data-bs-slide-to="0"
                    class="active"
                    aria-current="true"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleDark"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleDark"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                </div>
                <div class="carousel-inner">
                  <div class="carousel-item active" data-bs-interval="10000">
                    <img src={image} class="d-block w-100" alt="..." />
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style={{ color: "white" }}>Promoción 1</h5>
                      <p style={{ color: "white" }}>
                        Some representative placeholder content for the first
                        slide.
                      </p>
                    </div>
                  </div>
                  <div class="carousel-item" data-bs-interval="2000">
                    <img src={image} class="d-block w-100" alt="..." />
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style={{ color: "white" }}>Promoción 2</h5>
                      <p style={{ color: "white" }}>
                        Some representative placeholder content for the second
                        slide.
                      </p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src={image} class="d-block w-100" alt="..." />
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style={{ color: "white" }}>Promoción 3</h5>
                      <p style={{ color: "white" }}>
                        Some representative placeholder content for the third
                        slide.
                      </p>
                    </div>
                  </div>
                </div>
                <button
                  class="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleDark"
                  data-bs-slide="prev"
                >
                  <span
                    class="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button
                  class="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleDark"
                  data-bs-slide="next"
                >
                  <span
                    class="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex w-auto d-sm-block d-lg-none">
          <div class="card" style={{ border: "none", background: "none" }}>
            <div class="card-body">
              <a href="/menu">
                <img
                  style={{
                    width: "100px",
                    padding: "0px",
                    borderRadius: "10px",
                  }}
                  src={require("../../assets/Hot_Salmon_4.jpg")}
                  alt=""
                />
              </a>
            </div>
          </div>
          <div
            class="card"
            style={{ border: "none", background: "none", borderRadius: "10px" }}
          >
            <div class="card-body">
              <a href="/menu">
                <img
                  style={{
                    width: "100px",
                    padding: "0px",
                    borderRadius: "10px",
                  }}
                  src={require("../../assets/Hot_Salmon_4.jpg")}
                  alt=""
                />
              </a>
            </div>
          </div>
          <div class="card" style={{ border: "none", background: "none" }}>
            <div class="card-body">
              <a href="/menu">
                <img
                  style={{
                    width: "100px",
                    padding: "0px",
                    borderRadius: "10px",
                  }}
                  src={require("../../assets/Hot_Salmon_4.jpg")}
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}
