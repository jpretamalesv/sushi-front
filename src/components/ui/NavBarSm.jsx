import React from "react";
import { useDispatch, useSelector } from "react-redux";
import image from "../../assets/icon.png";
import { emptyCart } from "../../features/cartSlice";
import { ProductInCart } from "../../pages/Products/ProductInCart";
import * as bootstrap from "bootstrap";
import { useState } from "react";
import Modal from "./Modal";

export function NavBarSm() {
  const cart = useSelector((state) => state.cart);
  const [openModal, setModalOpen] = useState(false);
  const dispatch = useDispatch();
  return (
    <div
      className="d-flex w-100 h-100 flex-row"
      style={{ backgroundColor: "#0D0C1D" }}
    >
      <div className="d-flex h-100 w-25">
        <nav role="navigation">
          <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>

            <ul id="menu">
              <a href="/">
                <li>Inicio</li>
              </a>
              <a href="/login">
                <li>Login</li>
              </a>
              <a href="/menu">
                <li>Productos</li>
              </a>
              <a href="/">
                <li>Carrito</li>
              </a>
            </ul>
          </div>
        </nav>
      </div>
      <div class="d-flex w-75 justify-content-center">
        <div className="d-flex  align-items-center w-50 ">
          <a href="/">
            <img
              src="https://www.pngmart.com/files/Anime-Food-Download-PNG-Image.png"
              alt="Bootstrap"
              width="40"
              height="40"
            ></img>
            <span
              style={{
                color: "white",
                fontFamily: "Serif",
              }}
            >
              Fukusuke
            </span>
          </a>
        </div>
      </div>
      <button
        style={{
          backgroundColor: "#0D0C1D",
          borderColor: "#0D0C1D",
          color: "#A69CAC",
        }}
        type="button"
        class="btn btn-dark"
        data-bs-toggle="offcanvas"
        data-bs-target="#staticBackdrop"
        aria-controls="staticBackdrop"
      >
        <i class="fa fa-cart-shopping"></i>
      </button>

      <div
        class="offcanvas offcanvas-start"
        data-bs-backdrop="static"
        tabindex="-1"
        id="staticBackdrop"
        aria-labelledby="staticBackdropLabel"
      >
        <div class="offcanvas-header">
          <h4 class="offcanvas-title " id="staticBackdropLabel">
            Carrito de compras
          </h4>
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <div class="offcanvas-body">
          {cart.products.map((product) => (
            <div>
              <ProductInCart props={product} />
            </div>
          ))}
          {cart.products.length > 0 && (
            <div class="d-flex w-100 mt-4 justify-content-evenly p-0">
              <button
                style={{
                  borderRadius: "50%",
                  width: "38px",
                  background: "red",
                  border: "red",
                  height: "38px",
                }}
                onClick={() => setModalOpen(true)}
                type="button"
                class="btn btn-primary d-flex justify-content-center align-items-center"
              >
                <span>
                  <i class="fa-solid fa-x"></i>
                </span>
              </button>

              <Modal open={openModal} onClose={() => setModalOpen(false)}>
                ¿Desea continuar?
              </Modal>
              <button
                style={{
                  borderRadius: "50%",
                  width: "38px",
                  height: "38px",
                  background: "green",
                  border: "green",
                }}
                type="button"
                onClick={() => setModalOpen(true)}
                class="btn btn-primary d-flex align-items-center justify-content-center"
              >
                <span>
                  <i class="fa-solid fa-check"></i>
                </span>
              </button>
            </div>
          )}
          {cart.products.length === 0 && (
            <div className="d-flex h-100 justify-content-center align-items-center">
              <h5>El carrito está actualmente vacío</h5>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
