import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Switch,
  Routes,
  Route,
  Link,
} from "react-router-dom";
import './App.css';
import { Login } from './pages/Login/Login'
import { Home } from './pages/Home/HomeView'
import { Register } from './pages/Signup/Register';
import { Navbar } from 'react-bootstrap';
import { Footer } from './components/ui/Footer';
import { Menu } from './pages/Products/Menu';
import { ProfileEditor } from './pages/User/ProfileEditor';
import { useDispatch, useSelector } from 'react-redux';
import {AdminView} from './pages/Admin/AdminView'
import { ConfirmModal } from './components/ui/ConfirmModal';
import { RecoverPassword } from './pages/Signup/RecoverPassword';

function App() {
  
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  return (

    
      <Router>
        <Routes>
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/" element={<Home />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/recoverPassword" element={<RecoverPassword />} />
          <Route exact path="/menu" element={<Menu />} />
          <Route exact path="/userProfile" element={<ProfileEditor />} />
          <Route exact path="/admin" element={<AdminView/>} />
          {/* <Route path="*" element={<NotFound/>}/> */}
        </Routes>
      </Router>
  );
}

export default App;
