import React from "react";
import { useState } from "react";

export function AddressEditor() {
  // const [data, setData] = useState();
  // const getData = () => {
  //   fetch("http://localhost:1313/products/")
  //     .then((res) => res.json())
  //     .then((info) => setData(info));
  //   console.log(data);
  // };

  // getData();

  return (
    <div className="d-flex flex-column h-100 px-3 pt-3">
      <div className="d-flex flex-column h-75 w-100">
        <div class="d-flex flex-row justify-content-center w-100">
          <h3>Datos personales</h3>
        </div>
        <div className="d-flex flex-row mt-3  w-100 gap-3">
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">Direccion</label>
          </div>
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">Comuna</label>
          </div>
        </div>
        <div className="d-flex flex-row mt-3  w-100 gap-3">
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">Provincia</label>
          </div>
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">Region</label>
          </div>
        </div>
        <div className="d-flex flex-row mt-3  w-100 gap-3">
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">N° de apartamento</label>
          </div>
          <div class="form-floating mb-3 w-50">
            <input
              type="email"
              class="form-control"
              id="floatingInput"
              placeholder="name@example.com"
            />
            <label for="floatingInput">Celular</label>
          </div>
        </div>
      </div>

      <div className="d-flex flex-column h-25 justify-content-end mb-3">
        <button
          data-bs-toggle="modal"
          data-bs-target="#exampleModal"
          type="button"
          class="btn btn-primary confirmButton"
        >
          Guardar
        </button>
      </div>
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title fs-5" id="exampleModalLabel">
                ¿Esta seguro de realizar estos cambios?
              </h1>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div
              className="d-flex w-100 flex-row justify-content-around"
              style={{ padding: "10px" }}
            >
              <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="modal"
                style={{ border: "none", background: "red", width: "40%" }}
              >
                Cancelar
              </button>
              <button
                type="button"
                style={{ width: "40%" }}
                class="btn btn-primary confirmButton "
              >
                Aceptar
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
