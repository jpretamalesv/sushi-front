import React from "react";
import users from "../../data/user.json";
const ICON_STYLES = {
  cursor: "pointer",
  color: "white",
};
export function CRUDClient() {
  return (
    <>
      <div className="d-flex w-75 flex-column gap-3">
        <div className="d-flex justify-content-end">
          <i class="fa-solid fa-plus" style={{ color: "white" }}></i>
        </div>
        <table class="table table-hover" style={{ background: "white" }}>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre cliente</th>
              <th scope="col">Email</th>
              <th scope="col">RUT</th>
              <th scope="col">Direccion</th>
              <th scope="col">Telefono</th>
              <th scope="col">Sexo</th>
              <th scope="col">Contraseña</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => {
              return (
                <tr key={user.id}>
                  <th scope="row">{user.id}</th>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.rut}</td>
                  <td>{user.address}</td>
                  <td>{user.cellphone}</td>
                  <td>{user.gender}</td>
                  <td>{user.password}</td>
                  <td className="align-middle">
                    <div className="container d-flex align-items-center w-100 h-100 gap-1">
                      <i
                        style={{ cursor: "pointer", color: "grey" }}
                        class="fa-regular fa-eye"
                      ></i>
                      <i
                        style={{ cursor: "pointer", color: "grey" }}
                        class="fa-solid fa-pen-to-square"
                      ></i>
                      <i
                        style={{ cursor: "pointer", color: "grey" }}
                        class="fa-solid fa-trash"
                      ></i>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
