import React, { useState } from "react";
import { NavBar } from "../../components/ui/NavBar";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { Footer } from "../../components/ui/Footer";
import { ProductCard } from "../Products/ProductCard";
import { useDispatch, useSelector } from "react-redux";
import { ProductInCart } from "./ProductInCart";
import { emptyCart } from "../../features/cartSlice";
import * as bootstrap from "bootstrap";
export function Menu() {
  const productsEX = [
    {
      id: 0,
      name: "Handroll 1",
      price: 6000,
      description: "Camarón furai, salmón, cebollín envuelto en palta",
      img: require("../../assets/Crab_Ahumadooo.png"),
    },
    {
      id: 1,
      name: "Handro 2",
      price: 6300,
      description:
        "Camarón furai, salmón, queso crema, cebollín envuelto en nori tempurizado",
      img: require("../../assets/Gratinado_Sake_4.jpg"),
    },
    {
      id: 2,
      name: "Handroll 3",
      price: 5400,
      description:
        "Camarón furai, palta envuelto en palta, cubierto con crab tartar, pepino, ciboulette y acevichada ahumada",
      img: require("../../assets/Hot_Salmon_4.jpg"),
    },
    {
      id: 3,
      name: "Handroll 4",
      price: 5100,
      description:
        "Camarón furai o pollo furai, palta, envuelto en queso crema sopleteado, chimichurri, salsa unagui y papas hilo",
      img: require("../../assets/Niu_Hottt.png"),
    },
    {
      id: 4,
      name: "Handroll 5",
      price: 5000,
      description:
        "Camarón, queso crema, cebollín envuelto en palta, cubierto con cubos de pollo teriyaki y sésamo",
      img: require("../../assets/niu-cheese-rolljpg-246.jpg"),
    },
    {
      id: 5,
      name: "Handroll 6",
      price: 5600,
      description:
        "Salmón apanado, queso crema, palta, envuelto en palta, servido con salsa tari y unagui",
      img: require("../../assets/Special_Niu.png"),
    },
    {
      id: 6,
      name: "Handroll 7",
      price: 5400,
      description:
        "Camarón furai, palta, envuelto en salmón gratinado y salsa unagui",
      img: require("../../assets/SPICY_TEMPURA.jpg"),
    },
    {
      id: 7,
      name: "Handroll 8",
      price: 5300,
      description:
        "Salmón, camarón, queso crema envuelto en palta, salmón, mixto o crispys",
      img: require("../../assets/Tempura_Roll.jpg"),
    },
    {
      id: 8,
      name: "Handroll 9",
      price: 5100,
      description:
        "Camarón tempura, queso crema, cebollín cubierto en mixto de palta y atún, sésamo, masago, cibulette y salsa acevichada",
      img: require("../../assets/Teriyaki_Roll-4-Editar.jpg"),
    },
    {
      id: 9,
      name: "Handroll 10",
      price: 5000,
      description:
        "Camarón tempura, salsa spicy, queso crema, cebollín envuelto en palta, salmón, mixto o crispys",
      img: require("../../assets/TOKIO_WHITE.jpg"),
    },
    {
      id: 10,
      name: "Handroll 11",
      price: 5800,
      description:
        "Camarón tempura, queso crema, palta, envuelto en palta, salmón, mixto o crispys",
      img: require("../../assets/Crab_Ahumadooo.png"),
    },
    {
      id: 11,
      name: "Handroll 12",
      price: 5600,
      description:
        "Camarón, queso crema, palta envuelto en palta, salmón, mixto o crispys",
      img: require("../../assets/Chimichurri_Gratinado_7.jpg"),
    },
    {
      id: 12,
      name: "Handroll 13",
      price: 4900,
      description:
        "Pollo, queso crema, almendras tostadas envuelto en palta o crispys y salsa teriyaki",
      img: require("../../assets/DRAGON_ROLL.jpg"),
    },
    {
      id: 13,
      name: "Handroll 14",
      price: 5200,
      description: "Camarón tempura, cebollín envuelto en palta o crispys",
      img: require("../../assets/Special_Niu.png"),
    },
    {
      id: 14,
      name: "Handroll 15",
      price: 5600,
      description:
        "Salmón, palta, queso crema envuelto en palta, salmón, mixto o crispys",
      img: require("../../assets/Gratinado_Sake_4.jpg"),
    },
    {
      id: 15,
      name: "Handroll 16",
      price: 5800,
      description:
        "Salmón ahumado, palta, envuelto en queso crema y ciboulette",
      img: require("../../assets/Tempura_Roll.jpg"),
    },
  ];
  const toastTrigger = document.getElementById("liveToastBtn");
  const toastLiveExample = document.getElementById("liveToast");
  if (toastTrigger) {
    toastTrigger.addEventListener("click", () => {
      const toast = new bootstrap.Toast(toastLiveExample);

      toast.show();
    });
  }

  // const [products, setProducts] = useState([]);
  // const getProducts = () => {
  //   fetch("http://localhost:1313/products/")
  //     .then((res) => res.json())
  //     .then((productos) => setProducts(productos));
  //   console.log(products);
  // };

  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  // getProducts();

  return (
    <div class="d-flex flex-column gap-3 h-100 w-100 justify-content-between">
      <div className="d-flex d-none d-lg-block">
        <NavBar />
      </div>
      <div className="d-flex d-lg-none d-sm-block">
        <NavBarSm />
      </div>
      <div class="container-fluid text-center h-100">
        <div class="row h-100">
          <div class="col-2 h-100 d-none d-lg-block">
            <div className="d-flex w-100 justify-content-start flex-column h-100">
              <button
                disabled
                style={{ borderRadius: "0%" }}
                type="button"
                class="btn btn-primary"
              >
                Categorías
              </button>
              <button
                style={{ borderRadius: "0%" }}
                type="button"
                class="btn btn-primary"
              >
                Categoría 1
              </button>
              <button
                style={{ borderRadius: "0%" }}
                type="button"
                class="btn btn-primary"
              >
                Categoría 2
              </button>
              <button
                style={{ borderRadius: "0%" }}
                type="button"
                class="btn btn-primary"
              >
                Categoría 3
              </button>
              <button
                style={{ borderRadius: "0%" }}
                type="button"
                class="btn btn-primary"
              >
                Categoría 4
              </button>
            </div>
          </div>
          <div class="col w-auto d-flex h-100">
            <div
              id="style-3"
              className="d-flex flex-row flex-wrap overflow-y-auto justify-content-start  gap-1 w-auto"
              style={{
                background: "transparent",
                marginLeft: "2rem",
              }}
            >
              {productsEX.map((props) => {
                return <ProductCard key={props.id} product={props} />;
              })}
              {/* <div className="h-100 w-100">{products.name}</div> */}
            </div>
          </div>
        </div>
      </div>

      <div class="d-flex w-100">
        <Footer />
      </div>
    </div>
  );
}
