import React from "react";
import { Footer } from "../../components/ui/Footer";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { NavBar } from "../../components/ui/NavBar";
import { useState } from "react";

export function RecoverPassword() {
  const [isVisible, setVisible] = useState(true);
  return (
    <div className="d-flex flex-column h-100 w-100 justify-content-between">
      <div className="d-flex d-lg-block d-sm-none">
        <NavBar />
      </div>
      <div className="d-flex  d-sm-block d-lg-none ">
        <NavBarSm></NavBarSm>
      </div>
      <div className="d-flex w-100 h-100 justify-content-center align-items-center">
        {isVisible && (
          <div class="card w-auto h-auto" style={{ padding: "15px" }}>
            <div class="card-body">
              <div className="d-flex flex-column gap-3">
                <h3>Recuperar contraseña</h3>

                <div class="form-floating mb-3">
                  <input
                    type="email"
                    class="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                  />
                  <label for="floatingInput">Correo electrónico</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="email"
                    class="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                  />
                  <label for="floatingInput">RUT</label>
                </div>
                <button
                  onClick={() => setVisible(!isVisible)}
                  type="button"
                  class="btn btn-primary background"
                >
                  Enviar correo
                </button>
              </div>
            </div>
          </div>
        )}
        {!isVisible && (
          <div class="card w-auto h-auto" style={{ padding: "15px" }}>
            <div class="card-body">
              <div className="d-flex flex-column gap-3">
                <h3>Recuperar contraseña</h3>

                <div class="form-floating mb-3">
                  <input
                    type="email"
                    class="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                  />
                  <label for="floatingInput">Nueva contraseña</label>
                </div>
                <div class="form-floating mb-3">
                  <input
                    type="email"
                    class="form-control"
                    id="floatingInput"
                    placeholder="name@example.com"
                  />
                  <label for="floatingInput">Confirmar contraseña</label>
                </div>
                <button type="button" class="btn btn-primary background">
                  Enviar correo
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="d-flex">
        <Footer></Footer>
      </div>
    </div>
  );
}
