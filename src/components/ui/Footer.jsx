import React from "react";
import "../../assets/global.css";
export function Footer() {
  return (
    <div
      style={{ backgroundColor: "#0D0C1D" }}
      class="d-flex flex-column gap-2 pt-3 w-100"
    >
      <div class="d-flex flex-lg-row flex-column justify-content-evenly gap-2 ">
        <div class="d-flex flex-column gap-1">
          <h4
            class="d-flex justify-content-center"
            style={{ color: "#A69CAC" }}
          >
            Redes sociales
          </h4>
          <div class="d-flex flex-row justify-content-lg-evenly justify-content-center gap-3">
            <i
              style={{ color: "#3b5998", cursor: "pointer" }}
              class="fa-brands fa-facebook"
            ></i>
            <i
              style={{ color: "#1DA1F2", cursor: "pointer" }}
              class="fa-brands fa-twitter"
            ></i>
            <i
              style={{ color: "white", cursor: "pointer" }}
              class="fa-brands fa-tiktok"
            ></i>
          </div>
        </div>
        <div class="d-flex flex-column justify-content-center">
          <h4
            class="d-flex justify-content-center"
            style={{ color: "#A69CAC" }}
          >
            Información
          </h4>
          <div class="d-flex flex-column ">
            <div class="d-flex flex-row gap-1 justify-content-center">
              <span style={{ color: "white" }}>Direccion:</span>
              <a
                cla
                target="_blank"
                href="https://www.google.com/maps/dir/?api=1&destination=-33.4839116%2C-70.77106187&fbclid=IwAR0nhMuNARjobFNrygtEbazDG6x5pJ64AP40ozTaT80FCueu4Wab6jYhxBk"
              >
                Maps
              </a>
            </div>
            <div className="justify-content-center d-flex">
              <span style={{ color: "white" }}>Contacto: (2) 2762 5080</span>
            </div>
          </div>
        </div>
        <div class="d-flex flex-column gap-1">
          <h4
            class="d-flex justify-content-center"
            style={{ color: "#A69CAC" }}
          >
            Metodos de pago
          </h4>
          <div class="d-flex flex-row justify-content-center gap-3 justify-content-lg-evenly">
            <i
              style={{ color: "white" }}
              class="fa-brands fa-cc-mastercard"
            ></i>
            <i style={{ color: "white" }} class="fa-brands fa-cc-visa"></i>
            <i style={{ color: "white" }} class="fa-brands fa-cc-paypal"></i>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <span style={{ color: "white" }}>© 2022 Copyright: Fukusuke sushi</span>
      </div>
    </div>
  );
}
