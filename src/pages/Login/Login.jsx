import "../../assets/global.css";
import { React, useState } from "react";
import { NavBar } from "../../components/ui/NavBar";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { Footer } from "../../components/ui/Footer";

export function Login() {
  const [isVisible, setVisible] = useState(false);
  const toggle = () => {
    setVisible(!isVisible);
  };
  return (
    <div className="d-flex flex-column justify-content-between h-100">
      <div className="d-flex d-none d-lg-block">
        <NavBar />
      </div>
      <div className="d-flex d-lg-none d-sm-block">
        <NavBarSm />
      </div>
      <div class="d-flex justify-content-center px-5 px-lg-0 h-100 w-100 align-items-center">
        <div class="card align-items-center" style={{ width: "25rem" }}>
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <h5>Inicio de sesión</h5>
            </div>
            <div class="input-group mb-3">
              <input
                style={{ borderColor: "black" }}
                type="text"
                class="form-control"
                placeholder="Correo electronico"
                aria-label="Recipient's username"
                aria-describedby="button-addon2"
              ></input>
            </div>
            <div class="input-group mb-3">
              <input
                type={isVisible ? "text" : "password"}
                class="form-control"
                placeholder="Contraseña"
                style={{ borderRight: "none", borderColor: "black" }}
                aria-label="Recipient's username"
                aria-describedby="button-addon2"
              ></input>
              <button
                onClick={toggle}
                class="btn  btn-outline-secondary"
                style={{ borderLeft: "none", borderColor: "black" }}
                id="button-addon2"
              >
                <i
                  style={{ width: "1rem" }}
                  class={
                    isVisible ? "fa-solid fa-eye" : "fa-regular fa-eye-slash"
                  }
                ></i>
              </button>
            </div>
            <div class="d-flex flex-row justify-content-center">
              <div class="d-flex flex-column mb-2 w-100">
                <button type="button" class="btn btn-primary background">
                  Iniciar sesion
                </button>
                <div class="d-flex justify-content-center mb-1 mt-1">
                  <a href="/register">¿No tienes una cuenta?</a>
                </div>
                <div class="d-flex justify-content-center">
                  <a href="/recoverPassword">Recuperar contraseña</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
