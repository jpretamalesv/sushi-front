import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProduct } from "../../features/cartSlice";

export function ProductCard({ product }) {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  return (
    <div
      className="card"
      style={{
        width: "160px",
        height: "20rem",
        borderRadius: "5px 5px 10px 10px",
      }}
    >
      <div
        className="d-flex w-auto h-auto"
        style={{ justifyContent: "space-around" }}
      >
        <img
          style={{ width: "160px", height: "160px", marginTop: "-1px" }}
          src={product.img}
          className="card-img-top"
          alt="..."
        />
      </div>
      <div className="d-flex justify-content-around mt-1">
        <h5 className="card-title justify-content-between">{product.name}</h5>
        <h5 className="card-title justify-content-between">${product.price}</h5>
      </div>
      <div id="style-3" className="card-body scrollbar mb-1">
        <p className="card-text">
          <span style={{ color: "grey" }}>{product.description}</span>
        </p>
      </div>
      <div
        className="d-flex w-auto h-auto"
        style={{ justifyContent: "space-around" }}
      >
        {" "}
        <button
          onClick={() => dispatch(addProduct(product))}
          style={{ borderRadius: "0px 0px 10px 10px", width: "160px" }}
          className="btn btn-primary w-100 background"
        >
          {/* <i className="fa-solid fa-cart-shopping"></i> */}
          <span>Agregar al carro </span>
          <i className="fa-solid fa-plus"></i>
        </button>
      </div>
    </div>
  );
}
