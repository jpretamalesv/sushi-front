import React from "react";
import ReactDom from "react-dom";
import { ProductCard } from "../Products/ProductCard";
import axios from "axios";
import { useState } from "react";

const MODAL_STYLES = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "25px 25px 25px 25px",
  zIndex: 3000,
  borderRadius: "10px",
  width: "550px",
  border: "solid 1px black",
  justifyContent: "center",
};
const OVERLAY_STYLES = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,.7)",
  zIndex: 1000,
};

export default function AddProduct({ open, onClose }) {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");

  const [productForm, setForm] = {
    name: "",
    price: "",
    description: "",
  };
  const axios = require("axios");
  const [entrada, setEntrada] = useState({
    nombre: "",
    precio: "",
    descripcion: "",
  });
  const { nombre, precio, descripcion } = entrada;
  const [mensaje, setMensaje] = useState();
  const [loading, setLoading] = useState(false);
  const HandleChange = (e) => {
    setEntrada({ ...entrada, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (nombre !== "" && precio !== "" && descripcion !== "") {
      const product = {
        nombre,
        precio,
        descripcion,
      };
      setLoading(true);
      await axios
        .post("http://localhost:1313/productos/", product)
        .then((res) => {
          const { data } = res;
          setMensaje(data.mensaje);
          setEntrada({
            nombre: "",
            precio: "",
            descripcion: "",
          });
        })
        .catch((error) => {
          console.error(error);
          setMensaje("Hubo un error");
          setTimeout(() => {
            setMensaje("");
          }, 1500);
        });
    }
  };

  if (!open) return null;

  // const product = data.find((item) => item.id == productId);

  return ReactDom.createPortal(
    <>
      <div
        style={MODAL_STYLES}
        className="d-flex flex-row gap-3 mw-75 mw-lg-25"
      >
        <div className="d-flex flex-column  gap-2 mw-100 mw-lg-100">
          <div className="d-flex w-100 justify-content-between">
            <h4>Agregar producto</h4>
            <button
              onClick={onClose}
              type="button"
              class="btn-close"
              aria-label="Close"
            ></button>
          </div>
          <div className="d-flex flex-row justify-content-center">
            <form onSubmit={(e) => onSubmit(e)}>
              <div className="d-flex flex-column gap-2">
                <div className="d-flex flex-row gap-1">
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control"
                      id="floatingInput"
                      value={name}
                      name="nombre"
                      onChange={(e) => HandleChange(e)}
                    />
                    <label for="floatingInput">Nombre del producto</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control"
                      id="floatingInput"
                      value={price}
                      name="precio"
                      onChange={(e) => HandleChange(e)}
                    ></input>
                    <label for="floatingInput">Precio del producto</label>
                  </div>
                </div>
                <div class="form-floating">
                  <textarea
                    name="descripcion"
                    style={{ resize: "none", height: "200px" }}
                    class="form-control"
                    value={description}
                    id="floatingTextarea"
                    onChange={(e) => HandleChange(e)}
                  ></textarea>
                  <label for="floatingTextarea">Descripcion</label>
                </div>
              </div>
              <div className="d-flex flex-row mt-3 justify-content-between">
                <button
                  style={{ border: "none", background: "red", width: "95px" }}
                  type="button"
                  class="btn btn-primary"
                >
                  Cancelar
                </button>
                <button
                  style={{ width: "95px" }}
                  class="btn btn-primary confirmButton"
                  type="submit"
                >
                  Confirmar
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>,
    document.getElementById("portal")
  );
}
