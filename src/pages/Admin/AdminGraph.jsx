import React from "react";
export function AdminGraph() {
  return (
    <div>
      <h1>Ventas</h1>
      <div class="chart-container">
        <div class="base"></div>
        <ul class="meter">
          <li>
            <div>$100,000</div>
          </li>
          <li>
            <div>$80,000</div>
          </li>
          <li>
            <div>$60,000</div>
          </li>
          <li>
            <div>$40,000</div>
          </li>
          <li>
            <div>Less Than $20,000</div>
          </li>
        </ul>
        <table>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>

        <div class="bar one"></div>
        <div class="bar two"></div>
        <div class="bar three"></div>
        <div class="bar four"></div>
        <div class="bar five"></div>
        <div class="bar six"></div>
        <div class="bar seven"></div>
        <div class="bar eight"></div>
      </div>
    </div>
  );
}
