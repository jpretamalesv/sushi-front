import React from "react";
import orderData from "../../data/previousOrder.json";
export function PreviousOrders() {
  return (
    <div className="d-flex w-100 h-100  align-items-center">
      <table class="table table-hover" style={{ background: "white" }}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Productos</th>
            <th scope="col">Precio total</th>
            <th scope="col">Fecha</th>
            <th scope="col">Direccion</th>
            <th scope="col">Metodo de pago</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {orderData.map((data) => {
            return (
              <tr key={data.id}>
                <th scope="row">{data.id}</th>
                <td>{data.products}</td>
                <td>$ {data.totalPrice}</td>
                <td>{data.date}</td>
                <td>{data.address}</td>
                <td className="align-middle">{data.paymentMethod}</td>
                <td className="align-middle">
                  <div className="container d-flex align-items-center w-100 h-100 gap-3">
                    <i
                      style={{ cursor: "pointer", color: "grey" }}
                      class="fa-regular fa-eye"
                    ></i>
                    <i
                      style={{ cursor: "pointer", color: "grey" }}
                      class="fa-solid fa-repeat"
                    ></i>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
