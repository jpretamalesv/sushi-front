import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { minusQuantity, plusQuantity } from "../../features/cartSlice";

export function ProductInCart({ props }) {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  return (
    <div
      key={props.id}
      class="d-flex container justify-content-around align-items-center"
    >
      <div class="d-flex col-2  justify-content-center">
        <img
          src={props.img}
          style={{
            width: "30px",
            height: "30px",
            borderRadius: "50%",
          }}
          alt=""
        />
      </div>
      <div class="d-flex col-5  justify-content-center">{props.name}</div>
      <div class="d-flex col-5  justify-content-evenly ">
        <div class="container d-flex ">
          <div class="col-4 d-flex justify-content-center">
            <button
              onClick={() => dispatch(minusQuantity(props))}
              style={{
                borderRadius: "50%",
                width: "38px",
                background: "transparent",
                border: "transparent",
                height: "38px",
                color: "red",
              }}
              type="button"
              class="btn btn-primary d-flex justify-content-evenly align-items-center"
            >
              <span>
                <i class="fa-solid fa-minus"></i>
              </span>
            </button>
          </div>
          <div class="col-4 d-flex justify-content-center align-items-center">
            {props.cant}
          </div>
          <div class="col-4 d-flex justify-content-center">
            <button
              onClick={() => dispatch(plusQuantity(props))}
              style={{
                borderRadius: "50%",
                width: "38px",
                background: "transparent",
                border: "transparent",
                height: "38px",
                color: "green",
              }}
              type="button"
              class="btn btn-primary d-flex justify-content-center align-items-center"
            >
              <span>
                <i class="fa-solid fa-plus"></i>
              </span>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
