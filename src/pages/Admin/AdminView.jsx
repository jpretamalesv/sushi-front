import React from "react";
import { useState } from "react";
import { Footer } from "../../components/ui/Footer";
import { NavBar } from "../../components/ui/NavBar";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { AddProduct } from "./AddProduct";
import { AdminGraph } from "./AdminGraph";
import { CRUDClient } from "./CRUDClient";
import { CRUDProduct } from "./CRUDProduct";

export function AdminView() {
  const [editProfile, setVisible] = useState(false);
  const [editProduct, setVisible2] = useState(true);
  const [graphView, setGraphVisible] = useState(false);

  const toggleEditProduct = () => {
    setVisible2(true);
    setVisible(false);
    setGraphVisible(false);
  };
  const toggleEditProfile = () => {
    setVisible(true);
    setGraphVisible(false);
    setVisible2(false);
  };
  const toggleGraph = () => {
    setGraphVisible(true);
    setVisible(false);
    setVisible2(false);
  };
  return (
    <div className="d-flex flex-column h-100 justify-content-between">
      <div className="d-flex d-none d-lg-block">
        <NavBar />
      </div>
      <div className="d-flex d-lg-none d-sm-block">
        <NavBarSm />
      </div>
      <div className="d-flex flex-row justify-content-center align-items-center h-100 w-100">
        <div className="d-flex w-25 h-100 flex-column align-items-center justify-content-center">
          <button
            onClick={toggleEditProduct}
            style={{ width: "80%", borderRadius: "0%" }}
            type="button"
            class="btn btn-primary background"
          >
            Editar producto
          </button>
          <button
            onClick={toggleEditProfile}
            style={{ width: "80%", borderRadius: "0%" }}
            type="button"
            class="btn btn-primary background"
          >
            Editar cliente
          </button>
          <button
            onClick={toggleGraph}
            style={{ width: "80%", borderRadius: "0%" }}
            type="button"
            class="btn btn-primary background"
          >
            Estadísticas
          </button>
        </div>
        <div className="d-flex w-75 h-100">
          {editProduct && (
            <div class="d-flex align-items-center justify-content-center h-100 w-100">
              <CRUDProduct />
            </div>
          )}
          {editProfile && (
            <div class="d-flex align-items-center justify-content-center h-100 w-100">
              <CRUDClient />
            </div>
          )}
          {graphView && (
            <div class="d-flex align-items-center justify-content-center h-100 w-100">
              <AdminGraph />
            </div>
          )}
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
