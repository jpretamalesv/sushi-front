import React from "react";
import ReactDom from "react-dom";
import { ProductCard } from "../Products/ProductCard";
import data from "../../data/product.json";

const MODAL_STYLES = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%,-50%)",
  backgroundColor: "#FFF",
  padding: "20px 25px 20px 25px",
  zIndex: 3000,
  borderRadius: "10px",
  width: "500px",
  border: "solid 1px black",
  justifyContent: "center",
};
const OVERLAY_STYLES = {
  position: "fixed",
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
  backgroundColor: "rgba(0,0,0,.7)",
  zIndex: 1000,
};

export default function ViewProduct({ open, onClose, productId }) {
  if (!open) return null;
  const product = data.find((item) => item.id == productId);
  return ReactDom.createPortal(
    <>
      {product && (
        <div
          style={MODAL_STYLES}
          className="d-flex flex-row gap-3 mw-75 mw-lg-25"
        >
          <div className="d-flex flex-column  gap-2 mw-100 mw-lg-100">
            <div className="d-flex w-100 justify-content-between">
              <h4>ID Producto: # {product.id}</h4>
              <button
                onClick={onClose}
                type="button"
                class="btn-close"
                aria-label="Close"
              ></button>
            </div>
            <div className="d-flex flex-row justify-content-center">
              <div className="d-flex flex-column gap-2">
                <img src={product.image} alt="" />
                <div className="d-flex flex-row gap-1">
                  <div class="form-floating mb-3">
                    <input
                      disabled
                      type="email"
                      class="form-control"
                      id="floatingInput"
                      value={product.name}
                    />
                    <label for="floatingInput">Nombre del producto</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      disabled
                      type="email"
                      class="form-control"
                      id="floatingInput"
                      value={product.price}
                    ></input>
                    <label for="floatingInput">Precio del producto</label>
                  </div>
                </div>
                <div class="form-floating">
                  <textarea
                    disabled
                    style={{ resize: "none", height: "200px" }}
                    class="form-control"
                    value={product.description}
                    id="floatingTextarea"
                  ></textarea>
                  <label for="floatingTextarea">Descripcion</label>
                </div>
              </div>
            </div>
            <div className="d-flex flex-row justify-content-between">
              <div className="d-flex w-50 ">
                <button
                  style={{ border: "none", background: "red", width: "90%" }}
                  type="button"
                  class="btn btn-primary"
                >
                  Cancelar
                </button>
              </div>
              <div className="d-flex w-50 justify-content-end ">
                <button
                  style={{ width: "90%" }}
                  type="button"
                  class="btn btn-primary confirmButton"
                >
                  Confirmar
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>,
    document.getElementById("portal")
  );
}
