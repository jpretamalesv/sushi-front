import React from "react";
import { ProductCard } from "../Products/ProductCard";
import products from "../../data/product.json";
import EditProduct from "./EditProduct";
import ReadProduct from "./ReadProduct";
import AddProduct from "./AddProduct";
import Modal from "../../components/ui/Modal";
import { useState } from "react";
const ICON_STYLES = {
  cursor: "pointer",
  color: "white",
};
export function CRUDProduct() {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenRead, setIsOpenRead] = useState(false);
  const [isOpenAdd, setIsOpenAdd] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  let counter = 0;

  const handleOnClickEye = (product) => {
    setIsOpen(true);
    setIsOpenRead(false);
    setSelectedProduct(product);
  };
  const handleOnClickRead = (product) => {
    setIsOpenRead(true);
    setIsOpen(false);

    setSelectedProduct(product);
  };
  const handleOnClickAdd = (product) => {
    setIsOpenAdd(true);
    // setSelectedProduct(product);
  };

  const [productsCRUD, setProducts] = useState([]);
  const getProducts = () => {
    fetch("http://localhost:1313/products/")
      .then((res) => res.json())
      .then((productos) => setProducts(productos));
    // console.log(productsCRUD);
  };
  getProducts();

  return (
    <>
      <div className="d-flex w-75 flex-column">
        <div className="w-100 d-flex justify-content-end">
          <button
            onClick={handleOnClickAdd}
            style={{ background: "transparent", border: "none" }}
            type="button"
            class="btn btn-primary"
          >
            <i class="fa-solid fa-plus"></i>
          </button>
        </div>
        <table class="table table-hover" style={{ background: "white" }}>
          <thead>
            <tr>
              <th scope="col">#id</th>
              <th scope="col">Nombre Producto</th>
              <th scope="col">Precio</th>
              <th scope="col">Descripcion</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
            {/* {productsCRUD.products.map((product) => {
              counter += 1;
              return (
                <tr key={product._id}>
                  <th scope="row">{product._id}</th>
                  <td>{product.nombre}</td>
                  <td>$ {product.precio}</td>
                  <td>{product.descripcion}</td>
                  <td className="align-middle">
                    <div className="container d-flex align-items-center w-100 h-100 gap-1">
                      <i
                        onClick={() => handleOnClickRead(product._id)}
                        style={{ color: "grey" }}
                        class="fa-regular fa-eye"
                      ></i>
                      <i
                        onClick={() => handleOnClickEye(product._id)}
                        style={{ cursor: "pointer", color: "grey" }}
                        class="fa-solid fa-pen-to-square"
                      ></i>
                      <i
                        style={{ cursor: "pointer", color: "grey" }}
                        class="fa-solid fa-trash"
                      ></i>
                    </div>
                  </td>
                </tr>
              );
            })} */}
          </tbody>
        </table>
      </div>
      <EditProduct
        open={isOpen}
        onClose={() => setIsOpen(false)}
        productId={selectedProduct}
      ></EditProduct>
      <ReadProduct
        open={isOpenRead}
        onClose={() => setIsOpenRead(false)}
        productId={selectedProduct}
      />
      <AddProduct open={isOpenAdd} onClose={() => setIsOpenAdd(false)} />
    </>
  );
}
