import React from "react";

import { useState } from "react";
import { NavBar } from "../../components/ui/NavBar";
import { NavBarSm } from "../../components/ui/NavBarSm";
import { Footer } from "../../components/ui/Footer";

export function Register() {
  const [isVisible, setVisible] = useState(false);
  const toggle = () => {
    setVisible(!isVisible);
  };

  let steps = 0;
  function stepsHandler() {
    steps += 1;
  }
  return (
    <div class="d-flex flex-column h-100 w-100  justify-content-between">
      <div className="d-flex d-none d-lg-block">
        <NavBar />
      </div>
      <div className="d-flex d-lg-none d-sm-block">
        <NavBarSm />
      </div>
      <div class="d-flex px-3 px-lg-0 justify-content-center h-100 w-100 align-items-center">
        <div class="card" style={{ width: "35rem" }}>
          <div class="card-body">
            <div class="d-flex justify-content-center   mb-3">
              <h5 style={{ color: "gray" }}>Registro de sesión</h5>
            </div>
            <div class="d-flex flex-column gap-3 ">
              <div class="d-flex flex-row gap-3">
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Correo electronico"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Contraseña "
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
              </div>
              <div class="d-flex flex-row gap-3">
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="RUT"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Confirmar contraseña"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
              </div>
              <div class="d-flex flex-row gap-3">
                <div class="dropdown w-100">
                  <a
                    class="btn btn-secondary dropdown-toggle w-100 dropdown"
                    href="#"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Genero
                  </a>

                  <ul class="dropdown-menu">
                    <li>
                      <a class="dropdown-item" href="#">
                        Femenino
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#">
                        Masculino
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#">
                        Otro
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Teléfono"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
              </div>
              <div class="d-flex">
                <div class="input-group mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Dirección"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  ></input>
                </div>
              </div>
            </div>

            <div class="d-flex flex-row justify-content-center">
              <div class="d-flex flex-column mb-2 w-50">
                <button type="button" class="btn btn-primary background">
                  Confirmar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="d-flex flex-row w-100 h-auto">
        <Footer />
      </div>
    </div>
  );
}
